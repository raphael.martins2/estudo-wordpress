        <footer>
			<nav class="nav-footer">
			<ul>
				<li class="current_page_item"><a href="/">Menu</a></li>
				<li><a href="/sobre">Sobre</a></li>
				<li><a href="/contato">Contato</a></li>
			</ul>
			</nav>

			<p><?php echo bloginfo('name'); ?> © <?php echo date('Y'); ?>. Alguns direitos reservados.</p>
		</footer>
        
        <!-- Footer Wordpress -->
        <?php wp_footer(); ?>
        <!-- Fechar Footer Wordpress -->
	</body>
</html>